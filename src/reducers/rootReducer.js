import { ADD_ARTICLE } from "../constants/action-types";

const INITIAL_STATE = {
    articles: []
};

const rootReducer = (state = INITIAL_STATE, action) => {
    switch(action.type) {
        case ADD_ARTICLE:
            return {...state, articles: [...state.articles, action.payload] };
        default:
            return state;
    }
};

export default rootReducer;